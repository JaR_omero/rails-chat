require 'rails_helper'

RSpec.describe Room, type: :model do
  it 'can create valid room' do
    test_create(true, name: 'ro om')
  end

  it 'can\'t create invalid room with short name' do
    test_create(false, name: 'tes  ')
  end

  it 'can\'t create invalid room without name' do
    test_create(false, name: '')
    test_create(false, {})
  end

  it 'can\'t create two rooms with the same name' do
    Room.create!(name: 'test')
    expect{Room.create!(name: 'test')}.to raise_error(Mongoid::Errors::Validations)
    expect{Room.create!(name: 'TEST')}.to raise_error(Mongoid::Errors::Validations)
  end

  it 'can save' do
    room = Room.create!(name: 'room')
    expect(Room.count).to be(1)
    expect(room.created_at).to_not eq(nil)
    expect(room.updated_at).to_not eq(nil)
  end

  def test_create(valid, params)
    room = Room.new(params)
    expect(room.valid?).to be(valid)
  end
end
