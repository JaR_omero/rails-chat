require 'rails_helper'

RSpec.describe Message, type: :model do
  before(:each) do
    @room = Room.create!(name: 'room')
  end

  it 'can create valid message' do
    test_create(true, text: 't', username: 'u')
  end

  it 'can\'t create invalid message without text' do
    test_create(false, username: 'u', text: '')
    test_create(false, username: 'u')
  end

  it 'can\'t create invalid message without username' do
    test_create(false, text: 't', username: '')
    test_create(false, text: 't')
  end

  it 'can save' do
    msg = @room.messages.create!(text: 't', username: 'u')
    expect(@room.messages.count).to be(1)
    expect(msg.created_at).to_not eq(nil)
    expect(msg.updated_at).to_not eq(nil)
  end

  def test_create(valid, params)
    msg = @room.messages.build(params)
    expect(msg.valid?).to be(valid)
    msg
  end
end
