require 'rails_helper'

RSpec.describe User, type: :model do
  it 'can create valid user' do
    test_create(true, username: 'tes')
    test_create(true, username: 'ñ02A_')
    test_create(true, username: 't' * 15)
  end

  it 'can\'t create users with invalid usernames' do
    test_create(false, username: 'tést') # too short
    test_create(false, username: 'te') # too short
    test_create(false, username: ('t' * 16)) # too long
    test_create(false, username: '') # empty
    test_create(false, {}) # nil
  end

  def test_create(valid, params)
    user = User.new
    user.username = params[:username]
    expect(user.valid?).to be(valid)
  end
end
