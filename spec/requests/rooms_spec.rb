require 'rails_helper'

RSpec.describe 'Rooms endpoints', type: :request do
  describe 'POST /rooms' do
    it 'can create a room' do
      post rooms_path params: {name: 'test'}
      room = Room.first
      check_json_response(room, 201)
    end

    it 'expected error message when creates a room with a name in use' do
      post rooms_path params: {name: 'test'}
      post rooms_path params: {name: 'test'}
      check_json_response({errors: ['Name is already taken']}, 400)
    end

    it 'expected error messages when creates a room with an invalid name' do
      post rooms_path params: {name: 'te'}
      check_json_response('Name is too short', 400, false)
    end
  end

  describe 'GET /rooms' do
    it 'can paginate through the rooms ordered by last message' do
      29.times do |n|
        Room.create!(name: Faker::Lorem.characters(12))
      end

      create_messages_in_room 10, 2
      create_messages_in_room 28, 1

      Room.create!(name: 'First room')

      # First page
      get rooms_path
      expect(response).to have_http_status(200)

      content = json_response
      # Check total rooms and page rooms
      expect(content['total']).to eq(30)
      expect(content['rooms'].count).to eq(20)

      # Last room created appear first
      expect(content['rooms'][0]['name']).to eq('First room')

      # Rest of rooms appears ordered by last message created
      expect(content['rooms'][1]['messages'].count).to eq(1)
      expect(content['rooms'][2]['messages'].count).to eq(2)
      expect(content['rooms'][3]['messages']).to eq(nil)

      # Second page
      get rooms_path params: {page: 2}
      expect(response).to have_http_status(200)

      content = json_response
      expect(content['rooms'].count).to eq(10)
      expect(content['rooms'][0]['messages']).to eq(nil)

      # Third page
      get rooms_path params: {page: 3}
      expect(response).to have_http_status(200)

      content = json_response
      expect(content['rooms'].count).to eq(0)
    end
  end

  private

    def create_messages_in_room(room_index, num_of_messages)
      num_of_messages.times do |n|
        Room.all[room_index].messages.build(
            text: Faker::Lorem.words(3).join(" "), username: Faker::Lorem.characters(5)
        ).save
      end
    end
end
