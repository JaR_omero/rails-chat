require 'rails_helper'

RSpec.describe ApplicationCable::Connection, type: :channel do
  it 'successfully connects' do
    connect '/cable?username=test'
    expect(connection.username).to eq('test')
  end
end
