def create_connection(username, do_subscription = true)
  stub_connection username: username, random_id: SecureRandom::urlsafe_base64
  class << @connection
    attr_accessor :errors

    def connection_identifier
      [self.username, self.random_id].join(':')
    end
  end

  @connection.errors = CableHelper.check_username username

  ActionCable.server.add_connection(@connection)

  subscribe if do_subscription
end

def send_from_client(type, data = nil)
  perform :receive, type: type, data: data ? data.stringify_keys! : nil
end
