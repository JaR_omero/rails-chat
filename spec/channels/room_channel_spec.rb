require 'rails_helper'

RSpec.describe RoomChannel, type: :channel do
  describe 'Test new connections' do
    it 'can connect with valid username' do
      test_message_on_connection true, 'tes'
    end

    it 'can\'t connect with invalid username' do
      test_message_on_connection false, 'te'
    end

    it 'can\'t connect with username in use' do
      test_message_on_connection true, 'test'
      test_message_on_connection false, 'test', true
    end

    it 'don\'t count invalid usernames as "in use"' do
      test_message_on_connection false, 'te'
      test_message_on_connection false, 'te', false
    end
  end

  describe 'Subscription to a room' do
    it 'can subscribe to a room and receive the last messages and users' do
      room = Room.create!(name: 'room19')
      username = 'Luka'

      21.times do |n|
        room.messages.create!(text: n.to_s, username: username)
      end

      connect_and_subscribe_to_room 'test', room.id
      connect_and_subscribe_to_room username, room.id

      expect(streams.count).to eq(2)
      expect(streams).to include("room:#{room.id}")

      server_response = JSON.parse(get_server_messages(get_id).last)
      data = server_response['data']

      expect(server_response['type']).to eq(MessageTypes::TYPE_SUBSCRIBE_ROOM)

      # Check limit
      expect(data['messages'].count).to eq(20)

      # Check messages
      messages_id = data['messages'].map {|msg| msg['id']}
      expect(messages_id).not_to include(room.messages.first.id.to_s)
      expect(messages_id).to include(room.messages.last.id.to_s)

      # Check usernames
      usernames = data['users']
      expect(usernames.count).to be 2
      expect(usernames).to include username
      expect(usernames).to include 'test'

      # Check order
      expect(get_created_at(data, 0)).to be < get_created_at(data, 19)
      expect(room.messages.first.created_at).to be < get_created_at(data, 0)
    end

    it 'notify connections in a room' do
      room = Room.create!(name: 'room11')
      room_channel_id = "room:#{room.id}"

      subscription = connect_and_subscribe_to_room 'Geralt', room.id
      connect_and_subscribe_to_room 'Ratchet', room.id

      messages = get_server_messages room_channel_id
      expected = lambda do |username, type = MessageTypes::TYPE_CONNECTION|
        {type: type, data: {username: username}}.to_json
      end

      expect(messages[0]).to eq(expected.call 'Geralt')
      expect(messages[1]).to eq(expected.call 'Ratchet')

      subscription.unsubscribed

      last_message = get_server_messages(room_channel_id).last
      expect(last_message).to eq(expected.call 'Geralt', MessageTypes::TYPE_DISCONNECTION)

      # Disconnection requuest
      send_from_client MessageTypes::TYPE_UNSUBSCRIBE_ROOM, {room: room.id}
      last_message = get_server_messages(room_channel_id).last
      expect(last_message).to eq(expected.call 'Ratchet', MessageTypes::TYPE_DISCONNECTION)
    end

    it 'can unsubscribe from a room' do
      room = Room.create!(name: 'room19')

      create_connection 'Luka'

      send_from_client MessageTypes::TYPE_SUBSCRIBE_ROOM, {room: room.id}
      send_from_client MessageTypes::TYPE_UNSUBSCRIBE_ROOM

      expect(streams).to_not include("room:#{room.id}")
      expect(streams).to include(get_id)
    end

    it 'can\'t subscribe to a room that does not exist' do
      room_id = 'fjklqwnuioew'
      check_subscription_errors MessageTypes::TYPE_SUBSCRIBE_ROOM,
                                "Room #{room_id} not found",
                                {room: room_id}
    end

    it 'receive error if not send the room ID' do
      check_subscription_errors MessageTypes::TYPE_SUBSCRIBE_ROOM, 'Subscription needs a room id', nil
    end

    it 'receive error if send invalid type' do
      check_subscription_errors 'invalid', "Unknown action type \"invalid\"", nil
    end
  end

  describe 'Send messages' do
    it 'send a message to a room' do
      room = Room.create!(name: 'Route 66')
      room_channel_id = "room:#{room.id}"
      username = 'Widowmaker'
      text = 'I see you, do you see me?'

      connect_and_subscribe_to_room username, room.id

      send_from_client MessageTypes::TYPE_NEW_MESSAGE, {text: text}

      room.reload
      expect(room.messages.count).to eq(1)

      message_instance = room.messages.first

      message = get_server_messages(room_channel_id).last

      expect(message).to include(MessageTypes::TYPE_NEW_MESSAGE)
      expect(message).to include(username)
      expect(message).to include(text)
      expect(message).to include(message_instance.id)
    end

    it 'send a invalid message' do
      room = Room.create!(name: 'Route 66')

      connect_and_subscribe_to_room 'Tracer', room.id

      send_from_client MessageTypes::TYPE_NEW_MESSAGE, {text: ''}

      room.reload
      expect(room.messages.count).to eq(0)

      message = get_server_messages(get_id).last
      expect(JSON.parse(message)['data']['errors']).to eq(["Text can't be blank", "Text is too short (minimum is 1 character)"])
    end
  end

  private
    def connect_and_subscribe_to_room(username, room_id)
      subscription = create_connection username
      send_from_client MessageTypes::TYPE_SUBSCRIBE_ROOM, {room: room_id}
      subscription
    end

    # Connect, try to subscribe for a room and assert errors
    def check_subscription_errors(type_sended, error_msg, data)
      create_connection 'Luka'

      expected = {type: MessageTypes::TYPE_ERROR, data: {errors: [error_msg]}}

      expect do
        send_from_client type_sended, data
      end.to broadcast_to(get_id).from_channel(RoomChannel).with(expected)

      expect(streams.count).to eq(1)
      expect(streams).to include(get_id)
    end

    # Get a message from array and return converted to datetime the field 'created_at'
    def get_created_at(data, msg_index)
      data['messages'][msg_index]['created_at'].to_datetime
    end

    def get_server_messages(stream)
      ActionCable.server.pubsub.broadcasts stream
    end

    def get_id
      @connection.connection_identifier
    end

    # Connect with a username and assert response
    def test_message_on_connection(is_valid, username, expected_in_use = false)
      create_connection username, false
      expected = get_expected_response(is_valid, username, expected_in_use)
      expect{ subscribe.subscription_thread.join }.to broadcast_to(get_id).from_channel(RoomChannel).with(expected)
    end

    # Create the expected response after a connection
    def get_expected_response(is_valid, username, expected_in_use)
      return {type: MessageTypes::TYPE_CONNECTED, data: {username: username}} if is_valid
      user = User.new
      user.username = username
      user.valid?
      errors = expected_in_use ? ["Username already in use"] : user.errors.full_messages
      {type: MessageTypes::TYPE_ERROR, data: {errors: errors}}
    end
end
