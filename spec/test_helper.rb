# Assertions for expected response
# @param status_code [Integer] default 200
# @param expected_response [Hash|String|Any]
# @param strict_response [Boolean] check only if its included in the response, not if they are equals
def check_json_response(expected_response, status_code = 200, strict_response = true)
  expect(response).to have_http_status(status_code)

  matcher = strict_response ? eq(expected_response.to_json) : include(expected_response)
  expect(response.body).to matcher
end

def json_response
  JSON.parse response.body
end