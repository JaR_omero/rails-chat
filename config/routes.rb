Rails.application.routes.draw do
  mount ActionCable.server, at: '/cable'

  constraints format: :json do
    resources :rooms, only: [:index, :create]
  end
end
