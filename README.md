# Rails Chat

### Analysis (spanish)

[Read analysis](docs/index.md)

### Run (with Docker)

* ``cd`` to project folder and run:

```
cp .env.dist .env
```

* Set your environment variables in ``.env``

* Run containers and setup database

```
docker volume create --name mongo_data
docker-compose build
docker-compose up -d
docker-compose exec web bash
rails db:setup
```

* Run tests

```
rspec # after 'docker-compose exec web bash'
```

* After run tests, see coverage in ``coverage/index.html``

* Open the browser (PORT defined in .env)

[http://localhost:8080](http://localhost:8080)