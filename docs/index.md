# Análisis

## Librerías utilizadas

Además de los paquetes requeridos por las especificaciones (Rails, Actioncable, Rspec, Mongoid) se han añadido también los siguientes paquetes:

- **[database_cleaner](https://github.com/DatabaseCleaner/database_cleaner)** para limpiar la base de datos entre tests

- **[action-cable-testing](https://github.com/palkan/action-cable-testing)** para realizar tests con ActionCable

- **[simplecov](https://github.com/colszowka/simplecov)** para generar el análisis de cobertura de tests

- **[ruby-debug-ide](https://github.com/ruby-debug/ruby-debug-ide)** y **[debase](https://github.com/denofevil/debase)** para depurar la aplicación con Docker y RubyMine

- **[faker](https://github.com/stympy/faker)** para generar algunos datos aleatorios en los tests

- **[kaminari](https://github.com/kaminari/kaminari)** para la paginación de consultas con Mongoid

## Funcionalidades

El backend permite conectarse con un nombre de usuario, crear salas de chat y enviar mensajes en ellas, notificando a los usuarios conectados a las salas de los nuevos mensajes o conexiones en tiempo real.

### Rails

Se ha generado el nuevo proyecto con la opción *API-only* para evitar que se añadan paquetes innecesarios y que los generadores añadan archivos innecesarios. Además se ha desactivado *ActiveRecord*.

### Usuarios

Se han barajado varias opciones para el sistema de usuarios. Se pensó inicialmente en añadir un registro de usuarios y añadir autenticación con contraseña, pero esto no estaba en las especificaciones y restaba dificultad, ya que es más sencillo identificar a los usuarios de esta manera.

Una vez se decidió que los usuarios no se iban a registrar, se barajaron alternativas tanto para identificar a los usuarios como para guardar su información, viendo las posibilidades con actioncable, mongo o usando cookies, teniendo en cuenta que al ser una aplicación desacoplada y tratando de cumplir los principios REST, no se debe mantener una sesión abierta en el servidor (dejando a un lado la conexión por WS).

Basándonos en el funcionamiento del chat más famoso de España, [Terra](https://www.terrachat.es/), el cual no requiere login, se observa que se puede iniciar sesión con un nombre de usuario en cada pestaña del navegador, y que esta sesión finaliza al recargar la página o cerrar la pestaña. Tras esto se terminó de decidir que la sesión únicamente se mantendría abierta mediante la conexión por websockets.

Además, dado que los usuarios solo tienen un username y que ese registro no pertenece a ningún cliente, sino que puede ir rotando (un usuario puede elegir un username que pertenecía a otra persona si ya no está conectada) se ha decidido no persistir esta entidad en la base de datos. Debido a esto, para poder identificar el usuario que ha enviado un mensaje a una sala de chat, se almacena directamente el username en la entidad mensaje, evitando *lookups* para obtener el username relacionado o inconsistencias en los datos dado este sistema de usuarios volátil y el uso de una base de datos no relacional.

Usando únicamente las conexiones por websockets, el servidor comprueba los nombres de usuario en uso y los usuarios que hay conectados en cada sala, sin riesgo de dejar información residual (como nombres de usuario que aparezcan en uso después de haberse desconectado el usuario) si falla en algún momento el servidor.

### Tests

Se han realizado los tests utilizando Rspec con un coverage del 100%. Para ver las estadísticas de cobertura, se puede acceder a ``coverage/index.html`` que se genera después de ejecutar los tests.

Se ha hecho uso del paquete [action-cable-testing](https://github.com/palkan/action-cable-testing) dada la necesidad de testear la lógica relacionada con ActionCable, ya que aún no hay una solución oficial de Rails para este apartado (aunque hay un par de PR del autor de este paquete con ese propósito).

### Repositorio

Se ha usado el patrón de repositorio (*RoomRepository*) que actúa de envoltorio para la API de mongoid, para poder reutilizar código y a la vez reducir el acoplamiento de la aplicación con Mongo. 