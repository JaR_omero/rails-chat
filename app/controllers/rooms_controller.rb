class RoomsController < ApplicationController
  def index
    rooms = repo.last_rooms_with_activity params[:page]
    response = {total: repo.count, rooms: rooms}
    render json: response, status: :ok
  end

  def create
    room = Room.new(room_params)
    if repo.save room
      render json: room, status: :created
    else
      render json: {errors: repo.errors(room)}, status: :bad_request
    end
  end

  private
    def room_params
      params.permit(:name)
    end

    def repo
      @repo ||= RoomRepository.new
    end
end
