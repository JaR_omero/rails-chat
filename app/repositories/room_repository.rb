# Room repo
# Repository decouple mongoid logic from app
class RoomRepository
  delegate :count, to: Room

  def last_rooms_with_activity(page = 1)
    Room.order_by(['updated_at', :desc]).page(page)
  end

  def last_messages(room)
    room.messages.order_by(['created_at', :desc]).page(1).per(20).reverse
  end

  def new_message(room, username, text)
    room.messages.build(username: username, text: text)
  end

  def find(id)
    Room.find(id) rescue nil
  end

  def save(object)
    object.save
  end

  def errors(object)
    object.errors.full_messages
  end
end
