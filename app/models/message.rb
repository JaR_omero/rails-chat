class Message < ApplicationDocument
  field :text, type: String
  field :username, type: String
  embedded_in :room, touch: true

  validates :username, presence: true
  validates :text, presence: true, length: { minimum: 1, maximum: 2000 }

  def text=(text)
    self[:text] = text.strip
  end
end
