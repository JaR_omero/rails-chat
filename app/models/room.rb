class Room < ApplicationDocument
  field :name, type: String
  embeds_many :messages

  NAME_REGEX = /\A[\wñÑ ]+\z/

  validates :name, presence: true,
            length: { minimum: 4, maximum: 25 },
            uniqueness: { case_sensitive: false },
            format: { with: NAME_REGEX }

  def name=(name)
    self[:name] = name.split.join " "
  end
end
