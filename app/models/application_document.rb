class ApplicationDocument
  include Mongoid::Document
  include Mongoid::Timestamps

  def as_json(options = nil)
    attrs = super(options)
    attrs['id'] = attrs.delete('_id').to_s
    attrs
  end
end