# User model
# No Mongo document, class only for validates purposes
class User
  include ActiveModel::Validations

  attr_accessor :username

  USERNAME_REGEX = /\A[\wñÑ]+\z/

  validates :username, presence: true, length: { minimum: 3, maximum: 15 }, format: { with: USERNAME_REGEX }
end
