class CableHelper
  # Select valid usernames from remote connections
  def self.active_usernames
    ActionCable.server.connections.select{|con| con.errors.empty?}.map{|con| con.username}.uniq
  end

  # Send a json message to channel
  # @param channel [String] channel identifier
  # @param type [String] message type identifier
  # @param data [String|Hash|Array] data to send
  # @param channel_class [Class] classname to stream_for instead stream_from
  def self.send_message(channel, type, data = nil, channel_class = nil)
    message = {type: type, data: data}
    if channel_class
      channel_class.broadcast_to channel, message
    else
      ActionCable.server.broadcast channel, message
    end
  end

  def self.send_errors(channel, errors)
    send_message(channel, MessageTypes::TYPE_ERROR, {errors: errors})
  end

  def self.check_username(username)
    return ["Username already in use"] if active_usernames.include?(username)

    user = User.new
    user.username = username
    user.valid?

    # return validation errors
    user.errors.full_messages
  end
end