require_relative './cable_helper'

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :username, :random_id

    attr_accessor :errors

    # Connect without validating the username to send error messages if necessary
    # Added a random string to identify connections with the same username
    def connect
      self.username = request.params[:username]
      self.random_id = SecureRandom::urlsafe_base64
      self.errors = CableHelper.check_username username
    end
  end
end
