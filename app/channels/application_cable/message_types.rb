module MessageTypes
  TYPE_CONNECTED        = 'connected'
  TYPE_ERROR            = 'error'
  TYPE_SUBSCRIBE_ROOM   = 'subscribe_room'
  TYPE_UNSUBSCRIBE_ROOM = 'unsubscribe_room'
  TYPE_CONNECTION       = 'connection'
  TYPE_DISCONNECTION    = 'disconnection'
  TYPE_NEW_MESSAGE      = 'new_message'
end