require_relative './application_cable/message_types'

class RoomChannel < ApplicationCable::Channel
  delegate :send_message, :send_errors, to: CableHelper

  @@connected_users = Hash.new

  # Store ref to thread for send subscription response
  attr_reader :subscription_thread

  # Initial subscription for messages
  def subscribed
    stream_from get_id

    send_response_when_ready
  end

  def unsubscribed
    unsubscribe_from_rooms
  end

  # Handle messages received
  def receive(message)
    type = message['type']
    data = message['data']

    # Handle message types
    case type
    when MessageTypes::TYPE_SUBSCRIBE_ROOM
      subscribe_room data
    when MessageTypes::TYPE_UNSUBSCRIBE_ROOM
      unsubscribe_from_rooms
    when MessageTypes::TYPE_NEW_MESSAGE
      create_new_message data
    else
      send_errors get_id, ["Unknown action type \"#{type}\""]
    end
  end

  private
    def repo
      @repo ||= RoomRepository.new
    end

    # Returns connection ID
    def get_id
      @con_id ||= connection.connection_identifier
    end

    # Remove streams, only leave connection ID stream for direct messages to the user
    def unsubscribe_from_rooms
      # Can't remove only one stream. Remove all and subscribe again only to connection ID
      stop_all_streams
      stream_from get_id

      # Remove user from room and notify
      unless @room.nil?
        notify_connection true
        @@connected_users[@room.id].reject!{|username| username === self.username}
        @room = nil
      end
    end

    def stream_for_room room
      # Save actual room for this connection to avoid calculate from 'streams' in consecutive requests
      @room = room

      # Notify a new connection to connected users
      notify_connection

      # Suscribe for room messages
      stream_for room

      # Save username for this room
      room_id = room.id
      @@connected_users[room_id] = [] if @@connected_users[room_id].nil?
      @@connected_users[room_id] << username
    end

    # Notify connection or disconnection for an username in a room
    def notify_connection(disconnect = false)
      return if @room.nil?
      type = disconnect ? MessageTypes::TYPE_DISCONNECTION : MessageTypes::TYPE_CONNECTION
      send_message @room, type, {username: username}, RoomChannel
    end

    # Subscribe to a room and disconnect it from the rest
    # @param data [Hash] received data
    def subscribe_room(data)
      unsubscribe_from_rooms

      if data.nil? || data['room'].nil? || data['room'].blank?
        send_errors get_id, ['Subscription needs a room id']
        return
      end

      room = repo.find data['room']

      if room
        stream_for_room room
        send_message get_id, MessageTypes::TYPE_SUBSCRIBE_ROOM, {
            messages: repo.last_messages(room),
            users: @@connected_users[@room.id]
        }
      else
        send_errors get_id, ["Room #{data['room']} not found"]
      end
    end

    # Create and send a new message in a room
    def create_new_message(data)
      if @room.nil?
        send_errors get_id, ['You are not in a room']
      end

      text = data.nil? ? '' : data['text']

      message = repo.new_message @room, username, text

      if repo.save message
        send_message @room, MessageTypes::TYPE_NEW_MESSAGE, message, RoomChannel
      else
        send_errors get_id, repo.errors(message)
      end
    end

  def send_response_when_ready
    @subscription_thread = Thread.new do

      # Control timeout
      max_iterations = 30 # approx ~3 seconds
      iterations = 0

      # Wait until subscription is ready
      until subscription_confirmation_sent?
        sleep(0.1)

        if iterations >= max_iterations
          break
        end

        iterations += 1
      end

      # Send transmission anyway
      send_connection_response
    end
  end

  def send_connection_response
    # Connection response, send validations errors if is necessary
    errors = connection.errors
    if errors.count == 0
      send_message get_id, MessageTypes::TYPE_CONNECTED, {username: username}
    else
      send_errors get_id, errors
    end
  end
end
